﻿using _04.Interfaces.Interfaces;
using System;
using System.Linq;

namespace _04.Interfaces.Classes
{
    public class BudjetApplication : IBudgetApplication
    {
        private readonly ICurrencyConverter currencyConverter;
        private readonly ITransactionRepository transactionRepository;
        private readonly ITransactionParser transactionParser;

        public BudjetApplication(ITransactionRepository transactionRepository, ITransactionParser transactionParser, ICurrencyConverter currencyConverter)
        {
            this.currencyConverter = currencyConverter;
            this.transactionRepository = transactionRepository;
            this.transactionParser = transactionParser;
        }

        public void AddTransaction(string input)
        {
            var transaction = transactionParser.Parse(input);
            transactionRepository.AddTransaction(transaction);
        }

        public void OutputTransactions()
        {
            foreach (var transaction in transactionRepository.GetTransactions())
            {
                Console.WriteLine($"Amount: {transaction.Amount}, Date: {transaction.Date}");
            }
        }

        public void OutputBalanceInCurrency(string currencyCode)
        {
            var totalCurrencyAmount = new CurrencyAmount(currencyCode, 0);

            var amounts = transactionRepository.GetTransactions()
                .Select(t => t.Amount)
                .Select(a => a.CurrencyCode != currencyCode ? currencyConverter.ConvertCurrency(a, currencyCode) : a)
                .ToArray();

            var totalBalanceAmount = amounts.Aggregate(totalCurrencyAmount, (t, a) => t += a);

            Console.WriteLine($"Balance: {totalBalanceAmount} {currencyCode}");
        }
    }
}
