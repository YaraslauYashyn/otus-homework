﻿using _04.Interfaces.Interfaces;
using _04.Interfaces.TestApplications;
using System;

namespace _04.Interfaces.Classes
{
    public class ConsoleManager : IConsolePrinter, IConsoleReader
    {
        public void Print(string text)
            => Console.WriteLine(text);

        public string Read()
            => Console.ReadLine();
    }
}
