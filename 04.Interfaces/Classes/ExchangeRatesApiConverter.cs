﻿using _04.Interfaces.Interfaces;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;

namespace _04.Interfaces.Classes
{
    public class ExchangeRatesApiConverter : ICurrencyConverter
    {
        readonly HttpClient httpClient;
        readonly IMemoryCache memoryCache;
        readonly string apiKey;

        public ExchangeRatesApiConverter(HttpClient httpClient, IMemoryCache memoryCache, string apiKey)
        {
            this.httpClient = httpClient;
            this.memoryCache = memoryCache;
            this.apiKey = apiKey;
        }

        public async Task<ExchangeRatesApiResponse> GetExchangeRatesAsync()
        {
            try
            {
                Trace.TraceInformation("Request to exchange service is started");
                var response = await httpClient.GetAsync($"http://api.exchangeratesapi.io/v1/latest?access_key={ apiKey }");
                Trace.TraceInformation("Response from exchage service is received");
                response = response.EnsureSuccessStatusCode();
                var json = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<ExchangeRatesApiResponse>(json);
            }
            catch(Exception ex)
            {
                Trace.TraceInformation("Getting error when sending request to exchange service: " + ex.Message);
                throw;
            }
        }

        public ICurrencyAmount ConvertCurrency(ICurrencyAmount amount, string currencyCode)
            => ConvertCurrencyAsync(amount, currencyCode).Result;

        public async Task<ICurrencyAmount> ConvertCurrencyAsync(ICurrencyAmount amount, string currencyCode)
        {
            var cachedResponse = await memoryCache.GetOrCreateAsync("response", ce =>
            {
                ce.AbsoluteExpirationRelativeToNow = TimeSpan.FromSeconds(10);
                return GetExchangeRatesAsync();
            });
            var amountInBase = amount.Amount / (decimal)cachedResponse.Rates[amount.CurrencyCode];
            var targetAmount = amountInBase * (decimal)cachedResponse.Rates[currencyCode];
            return new CurrencyAmount(currencyCode, targetAmount);
        }
    }

    public class ExchangeRatesApiResponse
    {
        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("timestamp")]
        public long Timestamp { get; set; }

        [JsonProperty("base")]
        public string Base { get; set; }

        [JsonProperty("date")]
        public DateTimeOffset Date { get; set; }

        [JsonProperty("rates")]
        public Dictionary<string, double> Rates { get; set; }
    }
}
