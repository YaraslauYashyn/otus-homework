﻿using _04.Interfaces.Interfaces;
using System.IO;

namespace _04.Interfaces.Classes
{
    public class FileLocation : IFileLocation
    {
        public FileLocation(string fileName, string fileExtension, string filePath)
        {
            FileName = fileName;
            FileExtension = fileExtension;
            FilePath = filePath;

            FullFilePath = Path.Combine(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.FullName, FileName + FileExtension);
        }

        public string FileName { get; set; }
        public string FileExtension { get; set; }
        public string FilePath { get; set; }
        public string FullFilePath { get; }
    }
}
