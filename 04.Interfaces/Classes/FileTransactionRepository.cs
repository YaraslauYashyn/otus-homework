﻿using _04.Interfaces.Interfaces;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace _04.Interfaces.Classes
{
    public class FileTransactionRepository : ITransactionRepository, IFileClear, IFileRead, IFileWrite
    {
        private readonly IFileLocation fileLocation;

        public FileTransactionRepository(IFileLocation fileLocation)
        {
            this.fileLocation = fileLocation;

            if (CheckFile())
                ClearFile();
            else
                throw new Exception("File missing!");
        }

        public void AddTransaction(ITransaction transaction)
        {
            Trace.WriteLine("AddTransactionToFile");

            WriteToFile<ITransaction>(fileLocation.FullFilePath, transaction);
        }

        public ITransaction[] GetTransactions()
        {
            Trace.WriteLine("GetTransactionFromFile");

            return ReadFromFile<ITransaction>(fileLocation.FullFilePath);
        }

        public void WriteToFile<T>(string filePath, T transactionToWrite)
        {
            using Stream stream = File.Open(filePath, FileMode.Append);

            var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

            binaryFormatter.Serialize(stream, transactionToWrite);
        }

        public T[] ReadFromFile<T>(string filePath)
        {
            List<T> transactions = new List<T>();

            var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

            using (Stream stream = File.Open(filePath, FileMode.Open))
            {
                while (stream.Position < stream.Length)
                {
                    transactions.Add((T)binaryFormatter.Deserialize(stream));
                }
            }

            return transactions.ToArray();
        }

        public bool CheckFile()
            => File.Exists(fileLocation.FullFilePath);

        public void ClearFile()
            => File.WriteAllText(fileLocation.FullFilePath, string.Empty);
    }
}
