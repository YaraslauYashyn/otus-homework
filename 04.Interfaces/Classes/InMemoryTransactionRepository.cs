﻿using _04.Interfaces.Interfaces;
using System.Collections.Generic;

namespace _04.Interfaces.Classes
{
    public class InMemoryTransactionRepository : ITransactionRepository
    {
        private readonly List<ITransaction> transactions;

        public InMemoryTransactionRepository()
        {
            transactions = new List<ITransaction>();
        }

        public void AddTransaction(ITransaction transaction)
        {
            transactions.Add(transaction);
        }

        public ITransaction[] GetTransactions()
        {
            return transactions.ToArray();
        }
    }
}
