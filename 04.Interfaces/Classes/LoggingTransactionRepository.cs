﻿using _04.Interfaces.Interfaces;
using System.Diagnostics;

namespace _04.Interfaces.Classes
{
    public class LoggingTransactionRepository : ITransactionRepository
    {
        private readonly ITransactionRepository transactionRepository;

        public LoggingTransactionRepository(ITransactionRepository transactionRepository)
        {
            this.transactionRepository = transactionRepository;
        }

        public void AddTransaction(ITransaction transaction)
        {
            Trace.WriteLine("AddTransaction");

            transactionRepository.AddTransaction(transaction);
        }

        public ITransaction[] GetTransactions()
        {
            Trace.WriteLine("GetTransactions");

            return transactionRepository.GetTransactions();
        }
    }
}
