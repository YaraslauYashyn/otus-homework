﻿using _04.Interfaces.Entities;
using _04.Interfaces.Interfaces;
using System;
using System.Diagnostics;

namespace _04.Interfaces.Classes
{
    public class TransactionParser : ITransactionParser
    {
        public ITransaction Parse(string input)
        {
            try
            {
                var date = DateTimeOffset.Now;

                var splits = input.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);

                var typeCode = splits[0];

                var currencyAmount = new CurrencyAmount(splits[2], decimal.Parse(splits[1]));

                switch (typeCode)
                {
                    case "Трата": case "Expense":
                        return new Expense(currencyAmount, date, splits[3], splits[4]);
                    case "Приход": case "Income":
                        return new Income(currencyAmount, date, splits[3]);
                    case "Трансфер": case "Transfer":
                        return new Transfer(currencyAmount, date, splits[3], splits[4]);
                    default:
                        throw new NotImplementedException();
                }
            }
            catch(Exception ex)
            {
                Trace.TraceInformation("Getting error when trying to process transaction: " + ex.Message);
                throw;
            }
        }
    }
}
