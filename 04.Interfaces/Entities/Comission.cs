﻿using _04.Interfaces.Interfaces;
using System;

namespace _04.Interfaces.Entities
{
    public class Comission : ITransaction
    {
        public ITransaction OriginalTransaction { get; }

        public ICurrencyAmount Amount { get; }

        public DateTimeOffset Date { get; }

        public string StringView => throw new NotImplementedException();

        public override string ToString() 
            => $"Comission {Amount} for the transaction: {OriginalTransaction}";
    }
}
