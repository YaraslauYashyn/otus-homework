﻿using _04.Interfaces.Interfaces;
using System;

namespace _04.Interfaces.Entities
{
    [Serializable]
    public class Expense : ITransaction
    {
        public Expense(ICurrencyAmount amount, DateTimeOffset date,
            string category, string destination)
        {
            Amount = amount;
            Date = date;
            Category = category;
            Destination = destination;
        }

        public ICurrencyAmount Amount { get; }

        public DateTimeOffset Date { get; }

        public string Category { get; }

        public string Destination { get; }

        public string StringView { get; }

        public override string ToString()
            => $"Expense {Amount} to {Destination} upon category {Category}";
    }
}
