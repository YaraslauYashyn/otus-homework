﻿using _04.Interfaces.Interfaces;
using System;

namespace _04.Interfaces.Entities
{
    [Serializable]
    public class Income : ITransaction
    {
        public Income(ICurrencyAmount amount, DateTimeOffset date, string source)
        {
            Amount = amount;
            Date = date;
            Source = source;
        }
        public ICurrencyAmount Amount { get; }

        public DateTimeOffset Date { get; }

        public string Source { get; }

        public override string ToString() => $"Income {Amount} on {Date} from {Source}";
    }
}
