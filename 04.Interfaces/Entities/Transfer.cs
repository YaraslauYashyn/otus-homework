﻿using _04.Interfaces.Interfaces;
using System;

namespace _04.Interfaces.Entities
{
    [Serializable]
    public class Transfer : ITransaction
    {
        public Transfer(ICurrencyAmount amount, DateTimeOffset date, string destination, string message)
        {
            Amount = amount;
            Date = date;
            Destination = destination;
            Message = message;
        }
        public ICurrencyAmount Amount { get; }

        public DateTimeOffset Date { get; }

        public string Destination { get; }

        public string Message { get; }

        public override string ToString() 
            => $"Transfer {Amount} on {Date} for {Destination} with message {Message}";
    }
}
