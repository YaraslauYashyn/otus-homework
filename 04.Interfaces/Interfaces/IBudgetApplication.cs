﻿namespace _04.Interfaces.Interfaces
{
    public interface IBudgetApplication
    {
        void AddTransaction(string input);

        void OutputTransactions();

        void OutputBalanceInCurrency(string currencyCode);
    }
}
