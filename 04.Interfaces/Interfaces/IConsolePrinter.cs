﻿namespace _04.Interfaces.Interfaces
{
    public interface IConsolePrinter
    {
        public void Print(string text);
    }
}
