﻿namespace _04.Interfaces.Interfaces
{
    public interface IConsoleReader
    {
        string Read();
    }
}
