﻿namespace _04.Interfaces.Interfaces
{
    public interface ICurrencyAmount
    {
        string CurrencyCode { get; }

        decimal Amount { get; }
    }
}
