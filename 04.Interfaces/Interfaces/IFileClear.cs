﻿namespace _04.Interfaces.Interfaces
{
    public interface IFileClear
    {
        public bool CheckFile();

        public void ClearFile();
    }
}
