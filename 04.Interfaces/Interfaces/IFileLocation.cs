﻿namespace _04.Interfaces.Interfaces
{
    public interface IFileLocation
    {
        string FilePath { set; }

        string FileName { set; }

        string FileExtension { set; }

        string FullFilePath { get; }
    }
}
