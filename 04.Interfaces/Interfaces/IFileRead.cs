﻿namespace _04.Interfaces.Interfaces
{
    public interface IFileRead
    {
        T[] ReadFromFile<T>(string filePath);
    }
}
