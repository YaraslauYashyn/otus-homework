﻿namespace _04.Interfaces.Interfaces
{
    public interface IFileWrite
    {
        void WriteToFile<T>(string filePath, T transactionToWrite);
    }
}
