﻿using System;
using System.Runtime.Serialization;

namespace _04.Interfaces.Interfaces
{
    public interface ITransaction
    {
        DateTimeOffset Date { get; }

        ICurrencyAmount Amount { get; }
    }
}
