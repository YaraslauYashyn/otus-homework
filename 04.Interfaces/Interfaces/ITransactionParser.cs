﻿namespace _04.Interfaces.Interfaces
{
    public interface ITransactionParser
    {
        ITransaction Parse(string input);
    }
}
