﻿namespace _04.Interfaces.Interfaces
{
    public interface ITransactionRepository
    {
        void AddTransaction(ITransaction transaction);

        ITransaction[] GetTransactions();
    }
}
