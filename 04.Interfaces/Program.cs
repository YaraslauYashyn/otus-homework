﻿using _04.Interfaces.Classes;
using _04.Interfaces.Resources;
using _04.Interfaces.TestApplications;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Reflection;

namespace _04.Interfaces
{
    class Program
    {
        static void Main(string[] args)
        {
            ConsoleManager consoleManager = new ConsoleManager();

            try
            {
                consoleManager.Print(TextResources.WelcomeText);
                consoleManager.Print(TextResources.QuestionText);

                switch (consoleManager.Read())
                {
                    case "1":
                        TestConsoleApplication.Launch();
                        break;
                    case "2":
                        TestFileApplication.Launch();
                        break;
                    case "3":
                        TestManualApplication.Launch(consoleManager);
                        break;
                    default:
                        break;
                }

                consoleManager.Print(TextResources.WorkEndText);

                Console.ReadKey();
            }
            catch (Exception ex)
            {
                consoleManager.Print($"Во время работы приложения произошла ошибка{ex.Message}");
            }
        }
    }
}
