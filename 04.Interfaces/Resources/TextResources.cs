﻿namespace _04.Interfaces.Resources
{
    public static class TextResources
    {
        public static string WelcomeText { get; private set; } = "Здравствуйте. Вы находитесь в тестовом приложении Otus-Interfaces \r\n";

        public static string WorkEndText { get; private set; } = "Конец работы";

        public static string QuestionText { get; private set; } = "Пожалуйста, выберите 1 - для демонстрации тестовой работы приложения в консольном режиме с памятью приложения \r\n" +
            "2 - для демонстрации тестовой работы приложения с файловой памятью \r\n" +
            "3 - для ручного ввода данных в консоль \r\n" +
            "любую другую кнопку - для выхода из приложения";

        public static string UserCaseTip { get; private set; } = "Введите транзакции в одном из следующих вариантов: \r\n" +
            "Трата -400 RUB* Продукты Пятерочка \r\n" +
            "Приход +5500 RUB Аванс \r\n" +
            "Трансфер -1500 RUB Родители Перевод \r\n" +
            "* - валюта может быть введена в одном из общепринятых международных стандартов \r\n" +
            "0 - закончить ввод и выбрать валюту остатка \r\n";

        public static string CurrencyChoiceText { get; private set; } = "Введите валюту остатка (EUR, USD, RUB и т.д.)";
    }
}
