﻿using _04.Interfaces.Classes;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Diagnostics;
using System.Net.Http;

namespace _04.Interfaces.TestApplications
{
    public static class TestConsoleApplication
    {
        public static void Launch()
        {
            try
            {
                Trace.Listeners.Add(new ConsoleTraceListener());

                var currencyConverter = new ExchangeRatesApiConverter(new HttpClient(), new MemoryCache(new MemoryCacheOptions()), "a5cf9da55cb835d0a633a7825b3aa8b5");

                var transactionParser = new TransactionParser();

                var transactionRepository = new InMemoryTransactionRepository();

                var budgetApp = new BudjetApplication(transactionRepository, transactionParser, currencyConverter);

                budgetApp.AddTransaction("Трата -400 RUB Продукты Пятерочка");
                budgetApp.AddTransaction("Трата -2000 RUB Бензин IRBIS");
                budgetApp.AddTransaction("Трата -500 RUB Кафе Шоколадница");
                budgetApp.AddTransaction("Приход +5500 RUB Аванс");
                budgetApp.AddTransaction("Трансфер -1500 RUB Родители Перевод");

                budgetApp.OutputTransactions();

                budgetApp.OutputBalanceInCurrency("EUR");
            }
            catch(Exception ex)
            {
                Trace.WriteLine(ex.Message);
            }
        }
    }
}
