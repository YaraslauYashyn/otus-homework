﻿using _04.Interfaces.Classes;
using _04.Interfaces.Resources;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Diagnostics;
using System.Net.Http;

namespace _04.Interfaces.TestApplications
{
    public static class TestManualApplication
    {
        public static void Launch(ConsoleManager consoleManager)
        {
            try
            {
                Trace.Listeners.Add(new ConsoleTraceListener());

                var currencyConverter = new ExchangeRatesApiConverter(new HttpClient(), new MemoryCache(new MemoryCacheOptions()), "a5cf9da55cb835d0a633a7825b3aa8b5");

                var transactionParser = new TransactionParser();

                var transactionRepository = new InMemoryTransactionRepository();

                var budgetApp = new BudjetApplication(transactionRepository, transactionParser, currencyConverter);

                consoleManager.Print(TextResources.UserCaseTip);

                string input;
                do
                {
                    input = consoleManager.Read();
                    if (input.Equals("0"))
                        break;
                    else
                        budgetApp.AddTransaction(input);
                } while (true);

                budgetApp.OutputTransactions();

                consoleManager.Print(TextResources.CurrencyChoiceText);

                budgetApp.OutputBalanceInCurrency(consoleManager.Read());
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
            }
        }
    }
}
