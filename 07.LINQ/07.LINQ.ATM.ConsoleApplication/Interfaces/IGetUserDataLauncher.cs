﻿namespace _07.LINQ.ATM.ConsoleApplication.Interfaces
{
    public interface IGetUserDataLauncher
    {
        void GetUserDataByCredentials();

        void GetUsersWithSettedBalance();

        void GetUserAccounts();

        void GetUserAccountsWithHistory();

        void GetOperationsWithLinkedUser();
    }
}
