﻿using System.Collections.Generic;

namespace _07.LINQ.ATM.ConsoleApplication.Interfaces
{
    public interface IInputPrinter
    {
        public void Print(string text);

        public void PrintCollection<T>(IEnumerable<T> list);

        public void PrintItem<T>(T entity);
    }
}
