﻿namespace _07.LINQ.ATM.ConsoleApplication.Interfaces
{
    public interface IInputReader
    {
        string Read();
    }
}
