﻿using _07.LINQ.ATM.ConsoleApplication.Services;
using _07.LINQ.ATM.Core.Services;
using _07.LINQ.ATM.DataAccess;
using System;
using System.Linq;

using static _07.LINQ.ATM.ConsoleApplication.Resources.TextResources;

namespace _07.LINQ.ATM.ConsoleApplication
{
    class Program
    {
        static void Main()
        {
            var consoleManager = new ConsoleManager();

            try
            {
                consoleManager.Print(StartApplication);

                consoleManager.Print(StartInitialization);

                consoleManager.Print(EndInitialization);

                var launchService = new LaunchService(consoleManager, consoleManager, CreateATMManager());

                string userChoice;

                do
                {
                    consoleManager.Print(Environment.NewLine);

                    consoleManager.Print(ClientMenu);

                    userChoice = consoleManager.Read();

                    switch (userChoice)
                    {
                        case "1":
                            launchService.GetUserDataByCredentials();
                            break;
                        case "2":
                            launchService.GetUserAccounts();
                            break;
                        case "3":
                            launchService.GetUserAccountsWithHistory();
                            break;
                        case "4":
                            launchService.GetOperationsWithLinkedUser();
                            break;
                        case "5":
                            launchService.GetUsersWithSettedBalance();
                            break;
                    }
                }
                while (!userChoice.Equals("0"));

                consoleManager.Print(EndApplication);
            }
            catch(Exception ex)
            {
                consoleManager.Print($"Во время работы приложения произошла ошибка: {ex.Message}");
            }
        }

        static ATMManager CreateATMManager()
        {
            using var dataContext = new ATMDataContext();
            var users = dataContext.Users.ToList();
            var accounts = dataContext.Accounts.ToList();
            var history = dataContext.History.ToList();

            return new ATMManager(accounts, users, history);
        }
    }
}
