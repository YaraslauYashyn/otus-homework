﻿using System;

namespace _07.LINQ.ATM.ConsoleApplication.Resources
{
    public static class TextResources
    {
        public static string StartApplication { get; } = "Старт приложения-банкомата...";

        public static string EndApplication { get; } = "Завершение работы приложения-банкомата...";

        public static string StartInitialization { get; } = "Старт инициализация данных...";

        public static string EndInitialization { get; } = "Завершение инициализации данных...";

        public static string EmptyCollection { get; } = "Данные в списке отсутствуют";

        public static string EmptyItem { get; } = "Элементов по заданным параметрам не найдено";

        public static string ClientMenu { get; } = "Выберите желаемую операцию: " + Environment.NewLine +
            "1 - Вывод информации о заданном аккаунте по логину и паролю;" + Environment.NewLine +
            "2 - Вывод данных о всех счетах заданного пользователя;" + Environment.NewLine +
            "3 - Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту;" + Environment.NewLine +
            "4 - Вывод данных о всех операциях пополнения счёта с указанием владельца каждого счёта;" + Environment.NewLine +
            "5 - Вывод данных о всех пользователях у которых на счёте сумма больше N" + Environment.NewLine +
            "0 - выйти из приложения" + Environment.NewLine;

        public static string LoginInput { get; } = "Пожалуйста, введите логин: ";

        public static string PasswordInput { get; } = "Пожалуйста, введите пароль: ";

        public static string BalanceInput { get; } = "Пожалуйста, введите баланс: ";

        public static string IdInput { get; } = "Пожалуйста, введите идентификатор: ";

        public static string IncorrectCredentials { get; } = "Логин или пароль пустой...";

        public static string IncorrectBalance { get; } = "Введен некорректный баланс...";

        public static string IncorrectId { get; } = "Введен некорректный идентификатор...";
    }
}
