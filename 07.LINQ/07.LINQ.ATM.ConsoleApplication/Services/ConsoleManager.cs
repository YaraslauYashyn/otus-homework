﻿using _07.LINQ.ATM.ConsoleApplication.Interfaces;

using System;
using System.Collections.Generic;

using static _07.LINQ.ATM.ConsoleApplication.Resources.TextResources;

namespace _07.LINQ.ATM.ConsoleApplication.Services
{
    class ConsoleManager : IInputPrinter, IInputReader
    {
        public void Print(string text)
            => Console.WriteLine(text);

        public void PrintCollection<T>(IEnumerable<T> list)
        {
            if (!(list is ICollection<T> collectionList) || collectionList.Count == 0)
                Print(EmptyCollection);
            else
            {
                foreach (var item in collectionList)
                {
                    Print(item.ToString());
                }
            }
        }
        public void PrintItem<T>(T entity)
        {
            if (entity == null)
                Print(EmptyItem);
            else
                Print(entity.ToString());
        }

        public string Read()
            => Console.ReadLine();
    }
}
