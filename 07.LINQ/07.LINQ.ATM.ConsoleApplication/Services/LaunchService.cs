﻿using _07.LINQ.ATM.ConsoleApplication.Interfaces;
using _07.LINQ.ATM.Core.Interfaces;

using System;

using static _07.LINQ.ATM.ConsoleApplication.Resources.TextResources;
using static _07.LINQ.ATM.ConsoleApplication.Services.Validator;

namespace _07.LINQ.ATM.ConsoleApplication.Services
{
    public class LaunchService: IGetUserDataLauncher
    {
        public IInputReader Reader { get; set; }

        public IInputPrinter Printer { get; set; }

        public IGetUserData GetService { get; set; }
        public LaunchService(IInputReader Reader, IInputPrinter Printer, IGetUserData GetService)
        {
            this.Reader = Reader;
            this.Printer = Printer;
            this.GetService = GetService;
        }

        public void GetUserDataByCredentials()
        {
            Printer.Print(LoginInput);

            var login = Reader.Read();

            Printer.Print(PasswordInput);

            var password = Reader.Read();

            if (!IsTextFilled(login) || !IsTextFilled(password))
                Printer.Print(IncorrectCredentials);
            else
            {
                Printer.PrintItem(GetService.GetUserDataByCredentials(login, password));
            }
        }

        public void GetUsersWithSettedBalance()
        {
            Printer.Print(BalanceInput);

            var balance = Reader.Read();

            if (!IsBalanceCorrect(balance))
                Printer.Print(IncorrectBalance);
            else
            {
                Printer.PrintCollection(GetService.GetUsersWithSettedBalance(Convert.ToDecimal(balance)));
            }
        }

        public void GetUserAccounts()
        {
            Printer.Print(IdInput);

            var id = Reader.Read();

            if (!IsIntIdCorrect(id))
                Printer.Print(IncorrectId);
            else
            {
                Printer.PrintCollection(GetService.GetUserAccounts(Convert.ToInt32(id)));
            }
        }

        public void GetUserAccountsWithHistory()
        {
            Printer.Print(IdInput);

            var id = Reader.Read();

            if (!IsIntIdCorrect(id))
                Printer.Print(IncorrectId);
            else
            {
                var dict = GetService.GetUserAccountsWithHistory(Convert.ToInt32(id));

                if (dict == null || dict.Count == 0)
                    Printer.Print(EmptyCollection);
                else
                {
                    Printer.Print($"Был выбран клиент № {id}");

                    foreach (var item in dict)
                    {
                        Printer.PrintItem(item.Key);

                        Printer.Print("По заданному счету были проведены следующие операции: ");

                        Printer.PrintCollection(item.Value);

                        Printer.Print(Environment.NewLine);
                    }
                }
            }
        }

        public void GetOperationsWithLinkedUser()
        {
            Printer.PrintCollection(GetService.GetOperationsWithLinkedUser());
        }
    }
}
