﻿namespace _07.LINQ.ATM.ConsoleApplication.Services
{
    public static class Validator
    {
        public static bool IsTextFilled(string text)
            => !string.IsNullOrEmpty(text);

        public static bool IsBalanceCorrect(string balance)
            => decimal.TryParse(balance, out _);

        public static bool IsIntIdCorrect(string Id)
            => int.TryParse(Id, out int intId) && intId >= 0;
    }
}
