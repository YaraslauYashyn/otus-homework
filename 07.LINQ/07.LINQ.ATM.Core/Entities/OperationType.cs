﻿namespace _07.LINQ.ATM.Core.Entities
{
    public enum OperationType
    {
        InputCash = 1,
        OutputCash = 2
    }
}