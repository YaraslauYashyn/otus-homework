﻿using _07.LINQ.ATM.Core.Entities;
using System.Collections.Generic;

namespace _07.LINQ.ATM.Core.Interfaces
{
    public interface IBankEntities
    {
        public IEnumerable<Account> Accounts { get; }

        public IEnumerable<User> Users { get; }

        public IEnumerable<OperationsHistory> History { get; }
    }
}
