﻿using _07.LINQ.ATM.Core.Entities;
using System.Collections.Generic;

namespace _07.LINQ.ATM.Core.Interfaces
{
    public interface IGetUserData
    {
        User GetUserDataByCredentials(string login, string password);

        List<User> GetUsersWithSettedBalance(decimal balance);

        List<Account> GetUserAccounts(int userId);

        Dictionary<Account, List<OperationsHistory>> GetUserAccountsWithHistory(int userId);

        List<(OperationsHistory, User)> GetOperationsWithLinkedUser(OperationType operationType = OperationType.InputCash);
    }
}
