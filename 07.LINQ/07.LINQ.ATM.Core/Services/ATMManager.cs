﻿using System.Collections.Generic;
using System.Linq;
using _07.LINQ.ATM.Core.Entities;
using _07.LINQ.ATM.Core.Interfaces;

namespace _07.LINQ.ATM.Core.Services
{
    public class ATMManager : IBankEntities, IGetUserData
    {
        public IEnumerable<Account> Accounts { get; private set; }

        public IEnumerable<User> Users { get; private set; }

        public IEnumerable<OperationsHistory> History { get; private set; }

        public ATMManager(IEnumerable<Account> accounts, IEnumerable<User> users, IEnumerable<OperationsHistory> history)
        {
            Accounts = accounts;
            Users = users;
            History = history;
        }

        public User GetUserDataByCredentials(string login, string password)
            => Users.Where(u => u.Login == login && u.Password == password).FirstOrDefault();

        public List<User> GetUsersWithSettedBalance(decimal balance)
            => Users.Join(
                Accounts.Where(a => a.CashAll > balance),
                u => u.Id,
                a => a.UserId,
                (u, a) => u).Distinct().ToList();

        public List<Account> GetUserAccounts(int userId)
            => Accounts.Where(a => a.UserId == userId).ToList();

        public Dictionary<Account, List<OperationsHistory>> GetUserAccountsWithHistory(int userId)
            => Accounts.Where(a => a.UserId == userId).
                ToDictionary(a => a, o => History.Where(h => h.AccountId == o.Id).ToList());

        public List<(OperationsHistory, User)> GetOperationsWithLinkedUser(OperationType operationType)
        {
            return (from h in History
                    join a in Accounts on h.AccountId equals a.Id
                    join u in Users on a.UserId equals u.Id
                    where h.OperationType == operationType
                    select new { h, u }).AsEnumerable()
                          .Select(c => (c.h, c.u)).ToList();
        }
    }
}