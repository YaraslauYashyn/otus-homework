﻿namespace _08.Strings.Interfaces
{
    public interface IInputPrinter
    {
        public void Print(string text, string path = null);
    }
}
