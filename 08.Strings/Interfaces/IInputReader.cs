﻿namespace _08.Strings.Interfaces
{
    public interface IInputReader
    {
        string Read();
    }
}
