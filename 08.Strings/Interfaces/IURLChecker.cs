﻿namespace _08.Strings.Interfaces
{
    public interface IURLChecker
    {
        public bool CheckUrl(ref string url, out string response);

        public string CorrectUrl(string url);

        public bool CheckUrlForEmpty(string url);

        public bool CheckUrlForCorrect(string url);

        public bool CheckUrlForStart(string url);
    }
}
