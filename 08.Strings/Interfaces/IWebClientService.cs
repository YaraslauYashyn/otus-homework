﻿namespace _08.Strings.Interfaces
{
    public interface IWebClientService
    {
        public bool TryGetSource(string url, out string source);
    }
}
