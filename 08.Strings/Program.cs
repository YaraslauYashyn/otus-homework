﻿using _08.Strings.Services;
using System;
using System.Text;

using static _08.Strings.Resources.TextResources;
using static _08.Strings.Services.CustomValidator;

namespace _08.Strings
{
    class Program
    {
        static void Main()
        {
            Console.OutputEncoding = Encoding.UTF8;
            var consoleManager = new ConsoleManager();

            consoleManager.Print(StartApplication);

            var urlChecker = new URLChecker();

            var webClientService = new WebClientService();

            var fileManager = new FileManager();

            var launchService = new LaunchService(urlChecker, webClientService, fileManager);

            consoleManager.Print(ClientMenu);

            try
            {
                string userChoice;

                do
                {
                    userChoice = consoleManager.Read();

                    if (userChoice.Equals("0"))
                        break;

                    var processResult = launchService.PreProcess(ref userChoice, out string response);

                    consoleManager.Print(response);

                    if (!processResult)
                        continue;

                    consoleManager.Print(StartParsing);

                    processResult = launchService.ProcessParsing(userChoice, out string htmlSource);

                    if (!processResult)
                    {
                        consoleManager.Print(ErrorOnResponse);
                        continue;
                    }
                    else
                    {
                        consoleManager.Print(AppreciateSource);
                    }

                    var images = launchService.ProcessSourceForImages(htmlSource);

                    if(!IsTextFilled(images))
                    {
                        consoleManager.Print(EmptyImages);
                        continue;
                    }

                    launchService.ProcessFileRecording(images, userChoice);

                    consoleManager.Print(EndParsing);
                }
                while (true);
            }
            catch (Exception ex)
            {
                consoleManager.Print($"Во время работы приложения произошла ошибка: {ex.Message}");
            }
        }
    }
}
