﻿using System.Text.RegularExpressions;

namespace _08.Strings.RegularExpressions
{
    public static class CustomRegularExpressions
    {
        public static Regex CheckUrl { get; } = new Regex(@"^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$");

        public static Regex CheckFileName { get; } = new Regex("[^a-zA-Z0-9 -]");

        public static Regex GetImages { get; } = new Regex("<img.+?src=[\"'](.+?)[\"'].*?>");
    }
}
