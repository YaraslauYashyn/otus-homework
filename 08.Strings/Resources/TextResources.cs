﻿namespace _08.Strings.Resources
{
    public static class TextResources
    {
        public static string StartApplication { get; } = "Старт приложения...";

        public static string ClientMenu { get; } = "Пожалуйста, введите URL для поиска изображений или введите 0 для выхода из приложения: ";

        public static string ErrorOnUrl { get; } = "Указанный URL не соответствует стандарту. Пожалуйста, проверьте корректность и повторите ввод: ";

        public static string EmptyUrl { get; } = "Указан пустой URL. Пожалуйста, проверьте корректность и повторите ввод: ";

        public static string ErrorOnResponse { get; } = "Указанный URL ведет на несуществующий или недоступный источник. Пожалуйста, проверьте корректность и повторите ввод:";

        public static string AppreciateUrl { get; } = "URL указан корректно.";

        public static string AppreciateSource { get; } = "Данные по источнику получены.";

        public static string StartParsing { get; } = "Старт парсинга...";

        public static string EndParsing { get; } = "Парсинг успешно завершен. Полученные изображения записаны в файл в директории debug. Введите новый URL или 0:";

        public static string UrlUnsecureStart { get; } = "http://";

        public static string UrlSecureStart { get; } = "https://";

        public static string EmptyImages { get; } = "На заданном ресурсе не найдено ни одного изображения";
    }
}
