﻿using _08.Strings.Interfaces;
using System;


namespace _08.Strings.Services
{
    class ConsoleManager : IInputPrinter, IInputReader
    {
        public void Print(string text, string path = null) => Console.WriteLine(text);
        public string Read() => Console.ReadLine();
    }
}
