﻿using static _08.Strings.Resources.TextResources;
using static _08.Strings.RegularExpressions.CustomRegularExpressions;

namespace _08.Strings.Services
{
    public static class CustomValidator
    {
        public static bool IsTextFilled(string text)
            => !string.IsNullOrEmpty(text);

        public static bool IsUrlStartsFromHttp(string url)
            => url.StartsWith(UrlSecureStart) || url.StartsWith(UrlUnsecureStart);

        public static bool IsUrlCorrect(string url)
            => CheckUrl.IsMatch(url);
    }
}
