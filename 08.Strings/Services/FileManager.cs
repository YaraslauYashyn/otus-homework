﻿using _08.Strings.Interfaces;
using System;
using System.IO;

using static _08.Strings.RegularExpressions.CustomRegularExpressions;

namespace _08.Strings.Services
{
    class FileManager : IInputPrinter
    {
        public void Print(string text, string path = null) => File.WriteAllText(GenerateName(path), text);

        private string GenerateName(string path) => CheckFileName.Replace(("Images_" + path + "_" + DateTime.Now.ToString()), "_") + ".txt";
    }
}
