﻿using _08.Strings.Interfaces;
using System.Text;

namespace _08.Strings.Services
{
    public class LaunchService
    {
        public IURLChecker URLChecker { get; set; }

        public IWebClientService WebClientService { get; set; }

        public IInputPrinter FileManager { get; set; }

        public LaunchService(IURLChecker URLChecker, IWebClientService WebClientService, IInputPrinter FileManager)
        {
            this.URLChecker = URLChecker;
            this.WebClientService = WebClientService;
            this.FileManager = FileManager;
        }

        public bool PreProcess(ref string url, out string response) => URLChecker.CheckUrl(ref url, out response);

        public bool ProcessParsing(string url, out string source) => WebClientService.TryGetSource(url, out source);

        public void ProcessFileRecording(string text, string path) => FileManager.Print(text, path);
        
        public string ProcessSourceForImages(string htmlSource)
        {
            var images = new StringBuilder();

            foreach (var img in RegularExpressions.CustomRegularExpressions.GetImages.Matches(htmlSource))
            {
                images = images.AppendLine(img.ToString());
            }

            return images.ToString();
        }
    }
}
