﻿using _08.Strings.Interfaces;

using static _08.Strings.Resources.TextResources;
using static _08.Strings.Services.CustomValidator;

namespace _08.Strings.Services
{
    public class URLChecker: IURLChecker
    {
        public bool CheckUrl(ref string url, out string response)
        {
            if (!CheckUrlForEmpty(url))
            {
                response = EmptyUrl;
                return false;
            }
            else if (!CheckUrlForCorrect(url))
            {
                response = ErrorOnUrl;
                return false;
            }
            else if (!CheckUrlForStart(url))
            {
                url = CorrectUrl(url);
            }

            response = AppreciateUrl;
            return true;
        }

        public string CorrectUrl(string url) => UrlUnsecureStart + url;

        public bool CheckUrlForEmpty(string url) => IsTextFilled(url);

        public bool CheckUrlForCorrect(string url) => IsUrlCorrect(url);

        public bool CheckUrlForStart(string url) => IsUrlStartsFromHttp(url);
    }
}
