﻿using _08.Strings.Interfaces;
using System;
using System.Net;

namespace _08.Strings.Services
{
    public class WebClientService: IWebClientService
    {
        private static WebClient webClient;

        public static WebClient GetClient()
        {
            if (webClient == null)
                webClient = new WebClient();
            return webClient;
        }

        public bool TryGetSource(string url, out string source)
        {
            source = string.Empty;
            try
            {
                source = GetClient().DownloadString(url);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
