﻿namespace _10.Reflection.Entities
{
    public class FTestClass
    {
        public int I1 { get; set; }

        public int I2 { get; set; }

        public int I3 { get; set; }

        public int I4 { get; set; }

        public int I5 { get; set; }
    }
}
