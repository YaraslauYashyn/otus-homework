﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace _10.Reflection.Interfaces
{
    public interface IFileLoader<T>
    {
        public Task GenerateCsvFile(string path, string data);

        Task<List<T>> LoadFromFile(string path, ISerializer serializer);
    }
}
