﻿namespace _10.Reflection.Interfaces
{
    public interface ISerializer
    {
        string SerializeFromObjectToString(object inputObj);

        public object SerializeFromStringToObject(string inputStr);
    }
}
