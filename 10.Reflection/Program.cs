﻿using _10.Reflection.Entities;
using _10.Reflection.Services;
using Newtonsoft.Json;
using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace _10.Reflection
{
    class Program
    {
        public const int Iterations = 100;
        public const string TestFilePath = "TestData.csv";

        static async Task Main(string[] args)
        {
            var fileLoad = new FileLoader<FTestClass>();

            var customSerializer = new Serializer();

            var f = new FTestClass() { I1 = 2, I2 = 4, I3 = 6, I4 = 8, I5 = 16 };
            Console.WriteLine("Custom serialization starting");

            var customSerializationTime = SerializationService<FTestClass>.Serialize(f, Iterations, customSerializer);

            Console.WriteLine("Time of custom serialization: " + customSerializationTime);

            Console.WriteLine(Environment.NewLine);

            Console.WriteLine("NewtonJson serialization starting");

            var newtonSerializationTime = SerializationService<FTestClass>.Serialize(f, Iterations);

            Console.WriteLine("Time of NewtonJson serialization: " + newtonSerializationTime);

            Console.WriteLine("Starting file operation...");

            var fileResult = await SerializationService<FTestClass>.SerializeFromFile(f, TestFilePath, fileLoad, customSerializer);

            Console.WriteLine("File operation result: " + fileResult);

            Console.ReadKey();
        }
    }
}
