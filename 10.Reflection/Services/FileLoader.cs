﻿using _10.Reflection.Interfaces;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace _10.Reflection.Services
{
    public class FileLoader<T> : IFileLoader<T> where T : new()
    {
        public async Task GenerateCsvFile(string path, string data)
        {
            if (File.Exists(path))
            {
                File.Delete(path);
            }

            await using var sw = File.AppendText(path);

            await sw.WriteLineAsync(data);
        }

        public async Task<List<T>> LoadFromFile(string path, ISerializer serializer)
        {
            var result = new List<T>();

            using var sr = new StreamReader(path);

            while (!sr.EndOfStream)
            {
                var line = await sr.ReadLineAsync();
                result.Add((T)serializer.SerializeFromStringToObject(line));
            }

            return result;
        }
    }
}
