﻿using _10.Reflection.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;

namespace _10.Reflection.Services
{
    public class SerializationService<T>
    {
        public SerializationService(T processedClass, int iterationNumber)
        {
            ProcessedClass = processedClass;
            IterationNumber = iterationNumber;
        }

        public T ProcessedClass { get; set; }

        public int IterationNumber { get; set; }

        public static long Serialize(T processedClass, int iterationNumber, ISerializer serializer = null)
            => serializer != null ? CustomSerialization(processedClass, iterationNumber, serializer) : NewtonSerialization(processedClass, iterationNumber);

        public static long CustomSerialization(T processedClass, int iterationNumber, ISerializer serializer)
        {
            var sw = new Stopwatch();
            string outputStr;
            object result;

            sw.Start();

            for (int i = 0; i < iterationNumber; i++)
            {
                try
                {
                    outputStr = serializer.SerializeFromObjectToString(processedClass);
                    result = serializer.SerializeFromStringToObject(outputStr);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Exception has been thrown during custom serialization: " + ex.Message);
                }
            }

            sw.Stop();

            return sw.ElapsedMilliseconds;
        }

        public static long NewtonSerialization(T processedClass, int iterationNumber)
        {
            var sw = new Stopwatch();

            string outputStr;
            object result;

            sw.Start();

            for (int i = 0; i < iterationNumber; i++)
            {
                try
                {
                    outputStr = JsonConvert.SerializeObject(processedClass, Formatting.Indented);
                    result = JsonConvert.DeserializeObject<T>(outputStr);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Exception has been thrown during NewtonJson serialization: " + ex.Message);
                }
            }

            sw.Stop();

            return sw.ElapsedMilliseconds;
        }

        public async static Task<List<T>> SerializeFromFile(T testClass, string filePath, IFileLoader<T> fileLoad, ISerializer serializer)
        {
            try
            {
                await fileLoad.GenerateCsvFile(filePath, serializer.SerializeFromObjectToString(testClass));
                return await fileLoad.LoadFromFile(filePath, serializer);
            }
            catch(Exception ex)
            {
                Console.WriteLine("Exception has been thrown during file operations: " + ex.Message);
                return null;
            }
        }
    }
}
