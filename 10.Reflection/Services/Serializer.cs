﻿using _10.Reflection.Entities;
using _10.Reflection.Interfaces;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace _10.Reflection.Services
{
    public class Serializer: ISerializer
    {
        public string SerializeFromObjectToString(object inputObj)
        {
            var result = string.Empty;

            if (inputObj == null || !inputObj.GetType().IsClass)
                throw new ArgumentException("Object is empty or not a class");

            var type = inputObj.GetType();

            foreach (var field in type.GetProperties(BindingFlags.Public | BindingFlags.Instance))
            {
                result += $"{field.Name}:{field.GetValue(inputObj)};";
            }

            return result;
        }

        public object SerializeFromStringToObject(string inputStr)
        {
            if (string.IsNullOrEmpty(inputStr))
                throw new ArgumentException("String is empty or null");

            var outputObj = new FTestClass();

            var fieldsDict = new Dictionary<string, int>();

            var subStrings = inputStr.Split(";", StringSplitOptions.RemoveEmptyEntries);

            foreach (var pair in subStrings)    
            {
                string[] fieldNameValue = pair.Split(":");
                fieldsDict.Add(fieldNameValue[0], int.Parse(fieldNameValue[1]));
            }

            var fields = typeof(FTestClass).GetProperties(BindingFlags.Public | BindingFlags.Instance);

            foreach (var field in fields) 
            {
                if (fieldsDict.TryGetValue(field.Name, out int valueToSet))
                {
                    field.SetValue(outputObj, valueToSet);
                }
            }

            return outputObj;
        }
    }
}
