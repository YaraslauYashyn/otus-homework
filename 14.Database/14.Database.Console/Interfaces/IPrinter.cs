﻿namespace _14.Database.Console.Interfaces
{
    public interface IPrinter
    {
        public void Print(string toPrint);
    }
}
