﻿namespace _14.Database.Console.Interfaces
{
    public interface IReader
    {
        public string Read();
    }
}
