﻿using _14.Database.Console.Services;
using System;
using System.Threading.Tasks;

using static _14.Database.Console.Resources.TextResources;
using static _14.Database.Console.Services.ConnectionStringService;
using static _14.Database.Console.Services.DatabaseService;

namespace _14.Database.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            MainAsync(args).GetAwaiter().GetResult();
        }

        private static async Task MainAsync(string[] args)
        {
            var consoleManager = new ConsoleManager();

            try
            {
                consoleManager.Print(WelcomeMessage);

                consoleManager.Print(UsernameInput);
                var username = consoleManager.Read();

                consoleManager.Print(PasswordInput);
                var password = consoleManager.Read();

                consoleManager.Print(DatabaseQuestion);

                bool isDatabaseExists;
                string database;

                do
                {
                    var isDatabaseChoice = consoleManager.Read();

                    if (isDatabaseChoice.Equals("yes") || isDatabaseChoice.Equals("no"))
                    {
                        isDatabaseExists = isDatabaseChoice.Equals("yes");

                        consoleManager.Print(DatabaseInput);

                        database = consoleManager.Read();

                        break;
                    }
                }
                while (true);

                consoleManager.Print(ConnectionChecking);

                if (!TestConnection(ConnectionString(username, password, isDatabaseExists ? database : string.Empty)).ConfigureAwait(false).GetAwaiter().GetResult())
                {
                    consoleManager.Print(TestConnectionFailed);
                    return;
                }

                consoleManager.Print(SuccessfulConnection);

                if (!isDatabaseExists)
                {
                    await CreateDatabaseAsync(ConnectionString(), database, username);
                    consoleManager.Print(DatabaseCreating);
                    ConnectionString(username, password, database, true);
                }

                await ClearTablesAsync(ConnectionString());

                await ClearSequencesAsync(ConnectionString());

                await CreateTablesBatchAsync(ConnectionString());

                await CreateForeignKeysAsync(ConnectionString());

                await FillDataInTablesAsync(ConnectionString());

                consoleManager.Print(MainMenu);

                string userChoice;

                do
                {
                    userChoice = consoleManager.Read();

                    if (userChoice.Equals("0"))
                        break;

                    switch(userChoice)
                    {
                        case "1":
                            ReadClassTable(ConnectionString(), consoleManager);
                            break;
                        case "2":
                            ReadGroupTable(ConnectionString(), consoleManager);
                            break;
                        case "3":
                            ReadStudentTable(ConnectionString(), consoleManager);
                            break;
                        case "4":
                            ReadStudentInGroupTable(ConnectionString(), consoleManager);
                            break;
                        case "5":
                            ReadSubjectTable(ConnectionString(), consoleManager);
                            break;
                        case "6":
                            ReadTeacherTable(ConnectionString(), consoleManager);
                            break;
                        case "11":
                            consoleManager.Print(SpecifyTeacherId);
                            var teacherId = consoleManager.Read();
                            if (!int.TryParse(teacherId, out int teacherIdInt))
                            {
                                consoleManager.Print(IdIsNotInteger);
                                break;
                            }
                            consoleManager.Print(SpecifyGroupId);
                            var groupId = consoleManager.Read();
                            if(!int.TryParse(groupId, out int groupIdInt))
                            {
                                consoleManager.Print(IdIsNotInteger);
                                break;
                            }
                            consoleManager.Print(SpecifyDate);
                            var date = consoleManager.Read();
                            consoleManager.Print(SpecifyTheme);
                            var theme = consoleManager.Read();
                            await InsertNewClass(ConnectionString(), teacherIdInt, groupIdInt, date, theme);
                            break;
                        case "12":
                            consoleManager.Print(SpecifyGroupName);
                            var groupName = consoleManager.Read();
                            consoleManager.Print(SpecifySubjectId);
                            var subjectId = consoleManager.Read();
                            if (!int.TryParse(subjectId, out int subjectIdInt))
                            {
                                consoleManager.Print(IdIsNotInteger);
                                break;
                            }
                            await InsertNewGroup(ConnectionString(), groupName, subjectIdInt);
                            break;
                        case "13":
                            consoleManager.Print(SpecifyFirstName);
                            var firstNameStudent = consoleManager.Read();
                            consoleManager.Print(SpecifyLastName);
                            var lastNameStudent = consoleManager.Read();
                            await InsertNewStudent(ConnectionString(), firstNameStudent, lastNameStudent);
                            break;
                        case "14":
                            consoleManager.Print(SpecifyStudentId);
                            var studentId = consoleManager.Read();
                            if (!int.TryParse(studentId, out int studentIdInt))
                            {
                                consoleManager.Print(IdIsNotInteger);
                                break;
                            }
                            consoleManager.Print(SpecifyGroupId);
                            var groupForStudentId = consoleManager.Read();
                            if (!int.TryParse(groupForStudentId, out int groupForStudentIdInt))
                            {
                                consoleManager.Print(IdIsNotInteger);
                                break;
                            }
                            await InsertNewStudentInGroup(ConnectionString(), studentIdInt, groupForStudentIdInt);
                            break;
                        case "15":
                            consoleManager.Print(SpecifySubjectName);
                            var subjectName = consoleManager.Read();
                            await InsertNewSubject(ConnectionString(), subjectName);
                            break;
                        case "16":
                            consoleManager.Print(SpecifyFirstName);
                            var firstNameTeacher = consoleManager.Read();
                            consoleManager.Print(SpecifyLastName);
                            var lastNameTeacher = consoleManager.Read();
                            consoleManager.Print(SpecifyCompanyName);
                            var companyName = consoleManager.Read();
                            await InsertNewTeacher(ConnectionString(), firstNameTeacher, lastNameTeacher, companyName);
                            break;
                    }
                }
                while (true);
            }
            catch (Exception ex)
            {
                consoleManager.Print("Application has been failed with unhandled exception: " + ex.Message);
                return;
            }
        }
    }
}
