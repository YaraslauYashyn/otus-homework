﻿using System;

namespace _14.Database.Console.Resources
{
    public static class TextResources
    {
        public static string WelcomeMessage { get; set; } = "Hello. This is test application for PostgreSQL demonstration. Please specify your connection: ";

        public static string TestConnectionFailed { get; set; } = "Application could not connect to the specified database. Please check parameters and re-launch application";

        public static string UsernameInput { get; set; } = "Database server owner: ";

        public static string PasswordInput { get; set; } = "Database server password: ";

        public static string DatabaseQuestion { get; set; } = "Do you have existing database (yes/no)?";

        public static string DatabaseInput { get; set; } = "Please specify database name: ";

        public static string DatabaseCreating { get; set; } = "Database creating...";

        public static string ConnectionChecking { get; set; } = "Connection checking...";

        public static string SuccessfulConnection { get; set; } = "Successful connection...";

        public static string SpecifyFirstName { get; set; } = "Please, input first name: ";

        public static string SpecifyLastName { get; set; } = "Please, input last name: ";

        public static string SpecifyCompanyName { get; set; } = "Please, input company name: ";

        public static string SpecifyGroupName { get; set; } = "Please, input group name: ";

        public static string SpecifySubjectName { get; set; } = "Please, input subject name: ";

        public static string SpecifyTeacherId { get; set; } = "Please, select teacher number: ";

        public static string SpecifyGroupId { get; set; } = "Please, select group number: ";

        public static string SpecifyStudentId { get; set; } = "Please, select student number: ";

        public static string SpecifySubjectId { get; set; } = "Please, select subject number: ";

        public static string SpecifyDate { get; set; } = "Please, input date: ";

        public static string SpecifyTheme { get; set; } = "Please, input theme: ";

        public static string IdIsNotInteger { get; set; } = "Input identifier is not integer. Please, choose an option again.";

        public static string MainMenu { get; set; } = "Please select desirable option: " + Environment.NewLine +
            "1 - read class table" + Environment.NewLine +
            "2 - read group table" + Environment.NewLine +
            "3 - read student table" + Environment.NewLine +
            "4 - read studentInGroup table" + Environment.NewLine +
            "5 - read subject table" + Environment.NewLine +
            "6 - read teacher table" + Environment.NewLine +
            "11 - insert into class table" + Environment.NewLine +
            "12 - insert into group table" + Environment.NewLine +
            "13 - insert into student table" + Environment.NewLine +
            "14 - insert into studentInGroup table" + Environment.NewLine +
            "15 - insert into subject table" + Environment.NewLine +
            "16 - insert into teacher table" + Environment.NewLine +
            "0 - exit from application"
            ;
    }
}
