﻿namespace _14.Database.Console.Services
{
    public static class ConnectionStringService
    {
        private static readonly string connectionStringTemplateWithDatabase = "Host=localhost;Username={username};Password={password};Database={database}";

        private static readonly string connectionStringTemplateWithoutDatabase = "Host=localhost;Username={username};Password={password};";

        private static string connectionString;

        public static string ConnectionString(string owner = default, string password = default, string database = default, bool isReinitialize = false)
        {
            owner ??= string.Empty;
            password ??= string.Empty;
            database ??= string.Empty;

            if (connectionString == null || isReinitialize)
                connectionString = string.IsNullOrEmpty(database) ? InitializeConnection(owner, password) : InitializeConnection(owner, password, database);

            return connectionString;
        }

        private static string InitializeConnection(string owner, string password)
            => connectionStringTemplateWithoutDatabase.Replace("{username}", owner).Replace("{password}", password);

        private static string InitializeConnection(string owner, string password, string database)
            => connectionStringTemplateWithDatabase.Replace("{username}", owner).Replace("{password}", password).Replace("{database}", database);
    }
}
