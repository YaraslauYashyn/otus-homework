﻿using _14.Database.Console.Interfaces;

namespace _14.Database.Console.Services
{
    class ConsoleManager : IPrinter, IReader
    {
        public void Print(string toPrint)
            => System.Console.WriteLine(toPrint);

        public string Read()
            => System.Console.ReadLine();
    }
}
