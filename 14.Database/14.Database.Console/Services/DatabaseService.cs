﻿using _14.Database.Console.Interfaces;
using Npgsql;
using System;
using System.Threading.Tasks;

namespace _14.Database.Console.Services
{
    public static class DatabaseService
    {
        public async static Task<bool> TestConnection(string connectionString)
        {
            await using NpgsqlConnection conn = new NpgsqlConnection(connectionString);
            await conn.OpenAsync();
            return conn.State == System.Data.ConnectionState.Open;
        }
        public async static Task ClearTablesAsync(string connectionString)
        {
            try
            {
                using var connection = new NpgsqlConnection(connectionString);
                connection.Open();

                var sql = @"
                    DROP TABLE IF EXISTS studentInGroup CASCADE;
                    DROP TABLE IF EXISTS ""class"" CASCADE;
                    DROP TABLE IF EXISTS ""group"" CASCADE;
                    DROP TABLE IF EXISTS subject CASCADE;
                    DROP TABLE IF EXISTS student CASCADE;
                    DROP TABLE IF EXISTS teacher CASCADE;
                ";

                using var cmd = new NpgsqlCommand(sql, connection);

                await cmd.ExecuteNonQueryAsync();
            }
            catch (Exception ex)
            {
                System.Console.WriteLine($"During database cleaning script running exception was thrown: {ex.Message}");
                throw;
            }
        }

        public async static Task ClearSequencesAsync(string connectionString)
        {
            try
            {
                using var connection = new NpgsqlConnection(connectionString);
                connection.Open();

                var sql = @"
                    DROP SEQUENCE IF EXISTS subject_id_seq;;
                    DROP SEQUENCE IF EXISTS group_id_seq;
                    DROP SEQUENCE IF EXISTS student_id_seq;
                    DROP SEQUENCE IF EXISTS studentInGroup_id_seq;
                    DROP SEQUENCE IF EXISTS teacher_id_seq;
                    DROP SEQUENCE IF EXISTS class_id_seq;
                ";

                using var cmd = new NpgsqlCommand(sql, connection);

                await cmd.ExecuteNonQueryAsync();
            }
            catch (Exception ex)
            {
                System.Console.WriteLine($"During database cleaning script running exception was thrown: {ex.Message}");
                throw;
            }
        }

        public async static Task CreateDatabaseAsync(string connectionString, string database, string owner)
        {
            try
            {
                using var connection = new NpgsqlConnection(connectionString);
                connection.Open();

                //Not working with parametrized sql
                var sql = @$"
                    CREATE DATABASE ""{database}""
                    WITH
                    OWNER = {owner}
                    ENCODING = 'UTF8'
                    TABLESPACE = pg_default
                    CONNECTION LIMIT = -1;
                ";

                using var cmd = new NpgsqlCommand(sql, connection);

                await cmd.ExecuteNonQueryAsync();
            }
            catch (Exception ex)
            {
                System.Console.WriteLine($"During database creating script running exception was thrown: {ex.Message}");
                throw;
            }
        }

        public async static Task CreateTablesBatchAsync(string connectionString)
        {
            try
            {
                using var connection = new NpgsqlConnection(connectionString);
                connection.Open();

                var sql = @"
                    CREATE SEQUENCE subject_id_seq;
                    CREATE TABLE subject
                    (
                    	id BIGINT NOT NULL PRIMARY KEY DEFAULT NEXTVAL('subject_id_seq'),
                    	SubjectName VARCHAR(100) NOT NULL
                    );
                    
                    CREATE SEQUENCE group_id_seq;
                    CREATE TABLE ""group""
                    (
                        id BIGINT NOT NULL PRIMARY KEY DEFAULT NEXTVAL('group_id_seq'),
                        GroupName VARCHAR(50) NOT NULL,
                        SubjectId BIGINT NOT NULL
                    );

                    CREATE SEQUENCE student_id_seq;
                    CREATE TABLE student
                    (
                        id BIGINT NOT NULL PRIMARY KEY DEFAULT NEXTVAL('student_id_seq'),
                        FirstName VARCHAR(100) NOT NULL,
                        LastName VARCHAR(100) NOT NULL
                    );

                    CREATE SEQUENCE studentInGroup_id_seq;
                    CREATE TABLE studentInGroup
                    (
                        id BIGINT NOT NULL PRIMARY KEY DEFAULT NEXTVAL('studentInGroup_id_seq'),
                        StudentId BIGINT NOT NULL,
                        GroupId BIGINT NOT NULL
                    );

                    CREATE SEQUENCE teacher_id_seq;
                    CREATE TABLE teacher
                    (
                        id BIGINT NOT NULL PRIMARY KEY DEFAULT NEXTVAL('teacher_id_seq'),
                        FirstName VARCHAR(100) NOT NULL,
                        LastName VARCHAR(100) NOT NULL,
                        CompanyName VARCHAR(100)
                    );

                    CREATE SEQUENCE class_id_seq;
                    CREATE TABLE ""class""
                    (
                        id BIGINT NOT NULL PRIMARY KEY DEFAULT NEXTVAL('class_id_seq'),
                        TeacherId BIGINT NOT NULL,
                        GroupId BIGINT NOT NULL,
                        Date TIMESTAMP NOT NULL,
                        Theme VARCHAR(100)
                    );
                ";

                using var cmd = new NpgsqlCommand(sql, connection);

                await cmd.ExecuteNonQueryAsync();
            }
            catch (Exception ex)
            {
                System.Console.WriteLine($"During tables creating script running exception was thrown: {ex.Message}");
                throw;
            }
        }

        public async static Task CreateForeignKeysAsync(string connectionString)
        {
            try
            {
                using var connection = new NpgsqlConnection(connectionString);
                connection.Open();

                var sql = @"
                    ALTER TABLE ""group"" 
                    ADD CONSTRAINT groupsSubjectFK
                    FOREIGN KEY(SubjectId)
                    REFERENCES subject(Id);
                    
                    ALTER TABLE studentInGroup
                    ADD CONSTRAINT studentsGroupFK
                    FOREIGN KEY(GroupId)
                    REFERENCES ""group""(Id);
                    
                    ALTER TABLE studentInGroup
                    ADD CONSTRAINT groupsStudentFK
                    FOREIGN KEY(StudentId)
                    REFERENCES student(Id);
                    
                    ALTER TABLE ""class""
                    ADD CONSTRAINT classTeacherFK
                    FOREIGN KEY(TeacherId)
                    REFERENCES teacher(Id);
                    
                    ALTER TABLE ""class""
                    ADD CONSTRAINT classGroupFK
                    FOREIGN KEY(GroupId)
                    REFERENCES ""group""(Id);
                ";

                using var cmd = new NpgsqlCommand(sql, connection);

                await cmd.ExecuteNonQueryAsync();
            }
            catch (Exception ex)
            {
                System.Console.WriteLine($"During foreign keys creating script running exception was thrown: {ex.Message}");
                throw;
            }
        }

        public async static Task FillDataInTablesAsync(string connectionString)
        {
            try
            {
                using var connection = new NpgsqlConnection(connectionString);
                connection.Open();

                var sql = @"
                    INSERT INTO subject (SubjectName)
                    VALUES 
                    ('Java'),
                    ('C#'),
                    ('Python')
                    ON CONFLICT DO NOTHING;
                    
                    INSERT INTO ""group""(GroupName, SubjectId)
                    VALUES
                    ('Java_2021_04', 1),
                    ('C#_2021_08', 2)
                    ON CONFLICT DO NOTHING;
                    
                    INSERT INTO student(FirstName, LastName)
                    VALUES
                    ('Natalia', 'Kovaleva'),
                    ('Piotr', 'Alekseev'),
                    ('Irina', 'Petrova'),
                    ('Andrey', 'Romanov'),
                    ('Oleg', 'Ivanov')
                    ON CONFLICT DO NOTHING;
                    
                    INSERT INTO studentInGroup(StudentId, GroupId)
                    VALUES
                    (1, 1),
                    (2, 1),
                    (3, 1),
                    (4, 2),
                    (5, 2)
                    ON CONFLICT DO NOTHING;
                    
                    INSERT INTO teacher(FirstName, LastName, CompanyName)
                    VALUES
                    ('Maksim', 'Kuznetsov', 'Epam'),
                    ('Aleksey', 'Semenov', '1C')
                    ON CONFLICT DO NOTHING;
                    
                    INSERT INTO ""class""(TeacherId, GroupId, Date, Theme)
                    VALUES
                    (1, 1, '2021-05-01 20:00:00', 'Introdction in Java'),
                    (1, 1, '2021-05-05 20:00:00', 'Classes in Java'),
                    (2, 2, '2021-07-10 20:00:00', 'Introdction in C#'),
                    (2, 2, '2021-07-15 20:00:00', 'Interfaces in C#')
                    ON CONFLICT DO NOTHING;
                ";

                using var cmd = new NpgsqlCommand(sql, connection);

                await cmd.ExecuteNonQueryAsync();
            }
            catch (Exception ex)
            {
                System.Console.WriteLine($"During database filling script running exception was thrown: {ex.Message}");
                throw;
            }
        }

        public static void ReadClassTable(string connectionString, IPrinter printer)
        {
            try
            {
                using var connection = new NpgsqlConnection(connectionString);
                connection.Open();

                var sql = @"
                SELECT ROW_NUMBER () OVER (ORDER BY class.Id), class.Id, teacher.FirstName, teacher.LastName, ""group"".groupname, date, theme 
                FROM class
                INNER JOIN Teacher
                ON Teacher.Id = TeacherId
                INNER JOIN ""group""
                ON ""group"".Id = GroupId
                ";

                using var cmd = new NpgsqlCommand(sql, connection);

                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var rowNumber = reader.GetInt32(0).ToString();
                    var id = reader.GetInt32(1).ToString();
                    var teacherName = reader.GetString(2) + " " + reader.GetString(3);
                    var groupName = reader.GetString(4);
                    var date = reader.GetDateTime(5).ToString();
                    var theme = reader.GetString(6);

                    printer.Print($"Read: [Number={rowNumber}, id={id}, teacherName={teacherName}, groupName={groupName}, date={date}, theme={theme}");
                }
            }
            catch (Exception ex)
            {
                System.Console.WriteLine($"During insert script running exception was thrown: {ex.Message}");
                throw;
            }
        }

        public static void ReadGroupTable(string connectionString, IPrinter printer)
        {
            try
            {
                using var connection = new NpgsqlConnection(connectionString);
                connection.Open();

                var sql = @"
                SELECT ROW_NUMBER () OVER (ORDER BY ""group"".Id), ""group"".Id, groupName, subject.subjectName 
                FROM ""group""
                INNER JOIN subject
                ON subject.Id = subjectId
                ";

                using var cmd = new NpgsqlCommand(sql, connection);

                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var rowNumber = reader.GetInt32(0).ToString();
                    var id = reader.GetInt32(1).ToString();
                    var groupName = reader.GetString(2);
                    var subjectName = reader.GetString(3);

                    printer.Print($"Read: [Number={rowNumber}, id={id}, groupName={groupName}, subjectName={subjectName}");
                }
            }
            catch (Exception ex)
            {
                System.Console.WriteLine($"During insert script running exception was thrown: {ex.Message}");
                throw;
            }
        }

        public static void ReadStudentTable(string connectionString, IPrinter printer)
        {
            try
            {
                using var connection = new NpgsqlConnection(connectionString);
                connection.Open();

                var sql = @"
                SELECT ROW_NUMBER () OVER (ORDER BY Id), id, firstName, lastName 
                FROM student
                ";

                using var cmd = new NpgsqlCommand(sql, connection);

                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var rowNumber = reader.GetInt32(0).ToString();
                    var id = reader.GetInt32(1).ToString();
                    var studentName = reader.GetString(2) + " " + reader.GetString(3);

                    printer.Print($"Read: [Number={rowNumber}, id={id}, studentName={studentName}");
                }
            }
            catch (Exception ex)
            {
                System.Console.WriteLine($"During insert script running exception was thrown: {ex.Message}");
                throw;
            }
        }

        public static void ReadStudentInGroupTable(string connectionString, IPrinter printer)
        {
            try
            {
                using var connection = new NpgsqlConnection(connectionString);
                connection.Open();

                var sql = @"
                SELECT ROW_NUMBER () OVER (ORDER BY studentInGroup.Id), studentInGroup.Id, Student.firstName, Student.lastName, groupName 
                FROM studentInGroup
                INNER JOIN student
                ON Student.Id = StudentId
                INNER JOIN ""group""
                ON ""group"".Id = groupId
                ";

                using var cmd = new NpgsqlCommand(sql, connection);

                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var rowNumber = reader.GetInt32(0).ToString();
                    var id = reader.GetInt32(1).ToString();
                    var studentName = reader.GetString(2) + " " + reader.GetString(3);
                    var groupName = reader.GetString(4);

                    printer.Print($"Read: [Number={rowNumber}, id={id}, studentName={studentName}, groupName={groupName}");
                }
            }
            catch (Exception ex)
            {
                System.Console.WriteLine($"During insert script running exception was thrown: {ex.Message}");
                throw;
            }
        }

        public static void ReadSubjectTable(string connectionString, IPrinter printer)
        {
            try
            {
                using var connection = new NpgsqlConnection(connectionString);
                connection.Open();

                var sql = @"
                SELECT ROW_NUMBER () OVER (ORDER BY Id), id, subjectName 
                FROM subject
                ";

                using var cmd = new NpgsqlCommand(sql, connection);

                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var rowNumber = reader.GetInt32(0).ToString();
                    var id = reader.GetInt32(1).ToString();
                    var subjectName = reader.GetString(2);

                    printer.Print($"Read: [Number={rowNumber}, id={id}, subjectName={subjectName}");
                }
            }
            catch (Exception ex)
            {
                System.Console.WriteLine($"During insert script running exception was thrown: {ex.Message}");
                throw;
            }
        }

        public static void ReadTeacherTable(string connectionString, IPrinter printer)
        {
            try
            {
                using var connection = new NpgsqlConnection(connectionString);
                connection.Open();

                var sql = @"
                SELECT ROW_NUMBER () OVER (ORDER BY Id), id, firstName, lastName, companyName 
                FROM teacher
                ";

                using var cmd = new NpgsqlCommand(sql, connection);

                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var rowNumber = reader.GetInt32(0).ToString();
                    var id = reader.GetInt32(1).ToString();
                    var teacherName = reader.GetString(2) + " " + reader.GetString(3);
                    var companyName = reader.GetString(4);

                    printer.Print($"Read: [Number = {rowNumber}, id={id}, teacherName={teacherName}, companyName={companyName}");
                }
            }
            catch (Exception ex)
            {
                System.Console.WriteLine($"During insert script running exception was thrown: {ex.Message}");
                throw;
            }
        }

        public async static Task InsertNewClass(string connectionString, int teacherId, int groupId, string date, string theme)
        {
            try
            {
                using var connection = new NpgsqlConnection(connectionString);
                connection.Open();

                var sql = @"
                INSERT INTO class(TeacherId, GroupId, Date, Theme) 
                VALUES (:teacher_Id, :group_Id, :date, :theme);
                ";

                using var cmd = new NpgsqlCommand(sql, connection);
                var parameters = cmd.Parameters;
                parameters.Add(new NpgsqlParameter("teacher_Id", teacherId));
                parameters.Add(new NpgsqlParameter("group_Id", groupId));
                parameters.Add(new NpgsqlParameter("date", date ?? string.Empty));
                parameters.Add(new NpgsqlParameter("theme", theme ?? string.Empty));

                await cmd.ExecuteNonQueryAsync();

                System.Console.WriteLine("Record has been added");
            }
            catch (NpgsqlException npqex)
            {
                System.Console.WriteLine($"There is an issue when insert new record: {npqex.Message}");
            }
            catch (Exception ex)
            {
                System.Console.WriteLine($"During insert script running exception was thrown: {ex.Message}");
                throw;
            }
        }

        public async static Task InsertNewStudent(string connectionString, string firstName, string lastName)
        {
            try
            {
                using var connection = new NpgsqlConnection(connectionString);
                connection.Open();

                var sql = @"
                INSERT INTO student(firstName, lastName) 
                VALUES (:first_name, :last_name);
                ";

                using var cmd = new NpgsqlCommand(sql, connection);
                var parameters = cmd.Parameters;
                parameters.Add(new NpgsqlParameter("first_name", firstName ?? string.Empty));
                parameters.Add(new NpgsqlParameter("last_name", lastName ?? string.Empty));

                await cmd.ExecuteNonQueryAsync();

                System.Console.WriteLine("Record has been added");
            }
            catch (NpgsqlException npqex)
            {
                System.Console.WriteLine($"There is an issue when insert new record: {npqex.Message}");
            }
            catch (Exception ex)
            {
                System.Console.WriteLine($"During insert script running exception was thrown: {ex.Message}");
                throw;
            }
        }

        public async static Task InsertNewStudentInGroup(string connectionString, int studentId, int groupId)
        {
            try
            {
                using var connection = new NpgsqlConnection(connectionString);
                connection.Open();

                var sql = @"
                INSERT INTO studentInGroup(studentId, groupId) 
                VALUES (:student_id, :group_id);
                ";

                using var cmd = new NpgsqlCommand(sql, connection);
                var parameters = cmd.Parameters;
                parameters.Add(new NpgsqlParameter("student_id", studentId));
                parameters.Add(new NpgsqlParameter("group_id", groupId));

                await cmd.ExecuteNonQueryAsync();

                System.Console.WriteLine("Record has been added");
            }
            catch (NpgsqlException npqex)
            {
                System.Console.WriteLine($"There is an issue when insert new record: {npqex.Message}");
            }
            catch (Exception ex)
            {
                System.Console.WriteLine($"During insert script running exception was thrown: {ex.Message}");
                throw;
            }
        }

        public async static Task InsertNewSubject(string connectionString, string subjectName)
        {
            try
            {
                using var connection = new NpgsqlConnection(connectionString);
                connection.Open();

                var sql = @"
                INSERT INTO subject(subjectName) 
                VALUES (:subject_name);
                ";

                using var cmd = new NpgsqlCommand(sql, connection);
                var parameters = cmd.Parameters;
                parameters.Add(new NpgsqlParameter("subject_name", subjectName ?? string.Empty));

                await cmd.ExecuteNonQueryAsync();

                System.Console.WriteLine("Record has been added");
            }
            catch (NpgsqlException npqex)
            {
                System.Console.WriteLine($"There is an issue when insert new record: {npqex.Message}");
            }
            catch (Exception ex)
            {
                System.Console.WriteLine($"During insert script running exception was thrown: {ex.Message}");
                throw;
            }
        }

        public async static Task InsertNewGroup(string connectionString, string groupName, int subjectId)
        {
            try
            {
                using var connection = new NpgsqlConnection(connectionString);
                connection.Open();

                var sql = @"
                INSERT INTO ""group""(GroupName, SubjectId) 
                VALUES (:group_name, subject_id);
                ";

                using var cmd = new NpgsqlCommand(sql, connection);
                var parameters = cmd.Parameters;
                parameters.Add(new NpgsqlParameter("group_name", groupName ?? string.Empty));
                parameters.Add(new NpgsqlParameter("subject_id", subjectId));

                await cmd.ExecuteNonQueryAsync();

                System.Console.WriteLine("Record has been added");
            }
            catch (NpgsqlException npqex)
            {
                System.Console.WriteLine($"There is an issue when insert new record: {npqex.Message}");
            }
            catch (Exception ex)
            {
                System.Console.WriteLine($"During insert script running exception was thrown: {ex.Message}");
                throw;
            }
        }

        public async static Task InsertNewTeacher(string connectionString, string firstName, string lastName, string companyName)
        {
            try
            {
                using var connection = new NpgsqlConnection(connectionString);
                connection.Open();

                var sql = @"
                INSERT INTO teacher(firstName, lastName, companyName) 
                VALUES (:first_name, :last_name, :company_name);
                ";

                using var cmd = new NpgsqlCommand(sql, connection);
                var parameters = cmd.Parameters;
                parameters.Add(new NpgsqlParameter("first_name", firstName ?? string.Empty));
                parameters.Add(new NpgsqlParameter("last_name", lastName ?? string.Empty));
                parameters.Add(new NpgsqlParameter("company_name", companyName ?? string.Empty));

                await cmd.ExecuteNonQueryAsync();

                System.Console.WriteLine("Record has been added");
            }
            catch (NpgsqlException npqex)
            {
                System.Console.WriteLine($"There is an issue when insert new record: {npqex.Message}");
            }
            catch (Exception ex)
            {
                System.Console.WriteLine($"During insert script running exception was thrown: {ex.Message}");
                throw;
            }
        }
    }


}
