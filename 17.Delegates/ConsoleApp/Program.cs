﻿using System;
using System.Collections.Generic;

using static _17.Delegates.Resources.Constants;
using static _17.Delegates.Resources.TextResources;

namespace _17.Delegates
{
    class Program
    {
        static void Main()
        {
            var FileNames = new List<string>{PassportFileName, ApplicationFileName, PhotoFileName};

            using var receiver = new DocumentReceiver.DocumentReceiver(DirectoryName, TimeoutPeriod, FileNames);

            receiver.TimedOut += Receiver_TimedOut;
            receiver.DocumentsReady += Receiver_DocumentsReady;

            while (!IsToExit)
            {
            }
        }

        private static void Receiver_DocumentsReady(object sender, EventArgs e)
        {
            Console.WriteLine(DocumentsUploaded);
            IsToExit = true;
        }

        private static void Receiver_TimedOut(object sender, EventArgs e)
        {
            Console.WriteLine(DocumentsFailed);
            IsToExit = true;
        }

        private static bool IsToExit { get; set; }

    }
}
