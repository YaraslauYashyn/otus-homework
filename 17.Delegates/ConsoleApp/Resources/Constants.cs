﻿namespace _17.Delegates.Resources
{
    public static class Constants
    {
        public const int TimeoutPeriod = 20000;

        public const string DirectoryName = "Temp";

        public const string PassportFileName = "Паспорт.jpg";

        public const string ApplicationFileName = "Заявление.txt";

        public const string PhotoFileName = "Фото.jpg";
    }
}
