﻿namespace _17.Delegates.Resources
{
    public static class TextResources
    {
        public const string DocumentsUploaded = "Documents have been uploaded successfully";

        public const string DocumentsFailed = "Documents have not been uploaded within specified time";
    }
}
