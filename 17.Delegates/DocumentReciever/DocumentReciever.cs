﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Timers;

namespace DocumentReceiver
{
    public class DocumentReceiver : IDisposable
    {
        public event EventHandler<EventArgs> DocumentsReady;

        public event EventHandler<EventArgs> TimedOut;

        private readonly IEnumerable<string> fileNames;
        private bool isDocumentsReady;
        private readonly Timer timer;
        private readonly FileSystemWatcher watcher;

        public string TargetDirectory { get; }
        public DocumentReceiver(string targetDirectory, int waitingInterval, IEnumerable<string> fileNames)
        {
            TargetDirectory = targetDirectory;

            CheckAndCreateDirectory(TargetDirectory);

            this.fileNames = fileNames;

            watcher = new FileSystemWatcher(TargetDirectory);
            watcher.Changed += Watcher_Changed;
            watcher.EnableRaisingEvents = true;

            timer = new Timer(waitingInterval);
            timer.Elapsed += Timer_Elapsed;
            timer.Enabled = true;
        }

        public void CheckAndCreateDirectory(string targetDirectory)
            => Directory.CreateDirectory(targetDirectory);

        public void Dispose()
        {
            watcher.Changed -= Watcher_Changed;
            timer.Elapsed -= Timer_Elapsed;
        }

        protected virtual void OnDocumentsReady()
        {
            TurnoffTime();
            DocumentsReady?.Invoke(this, EventArgs.Empty);
        }

        protected virtual void OnTimedOut()
        {
            TurnoffTime();
            TimedOut?.Invoke(this, EventArgs.Empty);
        }

        private void TurnoffTime()
        {
            timer.Enabled = false;
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (!isDocumentsReady)
                OnTimedOut();
        }

        private void Watcher_Changed(object sender, FileSystemEventArgs e)
        {
            if (IsDocumentsReady)
                return;

            var files = Directory.EnumerateFiles(TargetDirectory).ToList();
            var result = true;
            foreach (var filename in fileNames)
                result = result && files.Contains(Path.Combine(TargetDirectory, filename));
            IsDocumentsReady = result;
        }

        private bool IsDocumentsReady
        {
            get => isDocumentsReady;
            set
            {
                isDocumentsReady = value;
                if (isDocumentsReady)
                {
                    watcher.EnableRaisingEvents = false;
                    OnDocumentsReady();
                }
            }
        }
    }

}
