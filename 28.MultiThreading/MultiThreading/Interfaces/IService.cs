﻿namespace MultiThreading.Interfaces
{
    public interface IService
    {
        public int UsualArraySum(int[] array);

        public int ParallelLinqArraySum(int[] array);

        public int ParallelThreadArraySum(int[] array);
    }
}
