﻿using MultiThreading.Service;
using System;

namespace MultiThreading
{
    class Program
    {
        public const int SmallLength = 100000;

        public const int MediumLength = 1000000;

        public const int BigLength = 10000000;

        static void Main()
        {
            try
            {
                var arrayInstance = new ProcessingArray(SmallLength);

                var timingService = new TimingDecorator(new CalculationService());

                ExecutionService.StartCalculation(timingService, arrayInstance.GetArray());

                arrayInstance.AssignCapacity(MediumLength);

                ExecutionService.StartCalculation(timingService, arrayInstance.GetArray());

                arrayInstance.AssignCapacity(BigLength);

                ExecutionService.StartCalculation(timingService, arrayInstance.GetArray());
            }
            catch(Exception ex)
            {
                Console.WriteLine($"Exception has been thrown during application run {ex.Message}");
            }
            finally
            {
                Console.ReadKey();
            }
        }
    }
}
