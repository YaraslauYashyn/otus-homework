﻿using MultiThreading.Interfaces;
using System;

namespace MultiThreading.Service
{
    abstract class BaseDecorator: IService
    {
        private IService _service;

        public BaseDecorator(IService service)
        {
            _service = service;
        }

        public virtual int ParallelLinqArraySum(int[] array)
            => _service != null ? _service.ParallelLinqArraySum(array) : throw new Exception("Service is not initialized");

        public virtual int ParallelThreadArraySum(int[] array)
            => _service != null ? _service.ParallelThreadArraySum(array) : throw new Exception("Service is not initialized");

        public virtual int UsualArraySum(int[] array)
            => _service != null ? _service.UsualArraySum(array) : throw new Exception("Service is not initialized");
    }
}
