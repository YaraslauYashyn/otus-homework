﻿using System.Collections.Generic;
using System.Linq;

namespace MultiThreading.Service
{
    public static class CalculationHelper
    {
        public static IEnumerable<IEnumerable<T>> Split<T>(this T[] array, int size)
        {
            for (var i = 0; i < array.Length / size; i++)
            {
                yield return array.Skip(i * size).Take(size);
            }
        }

        public static int GetSum(IEnumerable<int> array)
        {
            var sum = 0;

            foreach (var item in array)
            {
                sum += item;
            }

            return sum;
        }
    }
}
