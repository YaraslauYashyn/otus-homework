﻿using MultiThreading.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace MultiThreading.Service
{
    class CalculationService : IService
    {
        public int UsualArraySum(int[] array)
        {
            return CalculationHelper.GetSum(array);
        }

        public int ParallelLinqArraySum(int[] array)
        {
            return array.AsParallel().Sum();
        }

        public int ParallelThreadArraySum(int[] array)
        {
            var sum = 0;

            int threads = Environment.ProcessorCount;

            var threadList = new List<Thread>();

            foreach (var subArray in CalculationHelper.Split(array, array.Length / threads))
            {
                var thread = new Thread(() =>
                {
                    Interlocked.Add(ref sum, CalculationHelper.GetSum(subArray));
                });

                thread.Start();

                threadList.Add(thread);
            }

            threadList.ForEach(t => t.Join());

            return sum;
        }
    }
}
