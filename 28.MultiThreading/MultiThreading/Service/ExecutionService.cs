﻿using MultiThreading.Interfaces;
using System;

namespace MultiThreading.Service
{
    public static class ExecutionService
    {
        public static void StartCalculation(IService service, int[] array)
        {
            int sum = default;

            Console.WriteLine($"Start of calculation for array with length {array.Length}" + Environment.NewLine);

            Console.WriteLine("Usual array calculation");
            sum = service.UsualArraySum(array);
            Console.WriteLine($"End of calculation, sum is {sum}");

            Console.WriteLine("Parallel linq array calculation");
            sum = service.ParallelLinqArraySum(array);
            Console.WriteLine($"End of calculation, sum is {sum}");

            Console.WriteLine("Parallel thread array calculation");
            sum = service.ParallelThreadArraySum(array);
            Console.WriteLine($"End of calculation, sum is {sum}");

            Console.WriteLine(Environment.NewLine);
        }
    }
}
