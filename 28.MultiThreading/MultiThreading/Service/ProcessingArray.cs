﻿using System;

namespace MultiThreading.Service
{
    class ProcessingArray
    {
        private const int MaxValue = 100;
        private const int MinValue = 0;

        private static int[] array;
        private int Capacity { get; set; }

        public ProcessingArray(int capacity)
        {
            AssignCapacity(capacity);
        }

        public void AssignCapacity(int capacity)
        {
            if (capacity < 1)
                throw new ArgumentException("Int array couldn't have zero or negative capacity");

            Capacity = capacity;
        }

        public int[] GetArray()
        {
            if (array == null || array.Length == 0 || array.Length != Capacity)
                array = GenerateArray(Capacity);
            return array;
        }

        private static int[] GenerateArray(int capacity)
        {
            var tempArray = new int[capacity];

            var rand = new Random(Guid.NewGuid().GetHashCode());

            for (int i = 0; i < tempArray.Length; i++)
            {
                tempArray[i] = rand.Next(MinValue, MaxValue);
            }

            return tempArray;
        }
    }
}
