﻿using MultiThreading.Interfaces;
using System;
using System.Diagnostics;

namespace MultiThreading.Service
{
    class TimingDecorator: BaseDecorator
    {
        public TimingDecorator(IService service): base(service) {}

        private static Stopwatch _stopwatch;

        public static Stopwatch GetStopwatch()
        {
            if (_stopwatch == null)
                _stopwatch = new Stopwatch();

            return _stopwatch;
        }

        public void RefreshStopwatch()
        {
            GetStopwatch().Reset();
        }

        public override int UsualArraySum(int[] array)
        {
            GetStopwatch().Start();

            var result = base.UsualArraySum(array);

            GetStopwatch().Stop();

            Console.WriteLine($"Used time for usual sum calculating: {GetStopwatch().ElapsedMilliseconds} millisecond");

            RefreshStopwatch();

            return result;
        }

        public override int ParallelLinqArraySum(int[] array)
        {
            GetStopwatch().Start();

            var result = base.ParallelLinqArraySum(array);

            GetStopwatch().Stop();

            Console.WriteLine($"Used time for parallel LINQ sum calculating: {GetStopwatch().ElapsedMilliseconds} millisecond");

            RefreshStopwatch();

            return result;
        }

        public override int ParallelThreadArraySum(int[] array)
        {
            GetStopwatch().Start();

            var result = base.ParallelThreadArraySum(array);

            GetStopwatch().Stop();

            Console.WriteLine($"Used time for parallel thread sum calculating: {GetStopwatch().ElapsedMilliseconds} millisecond");

            RefreshStopwatch();

            return result;
        }
    }
}
