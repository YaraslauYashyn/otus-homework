﻿using PrototypePattern.Enums;
using PrototypePattern.Interfaces;
using System;

namespace PrototypePattern.Entities
{
    public class Bicycle : Transport, IMyCloneable<Bicycle>, ICloneable
    {
        public Bicycle(string name, TransportType type, int wheels) : base(name, type)
        {
            Wheels = wheels;
        }

        public int Wheels { get; set; }

        public new object Clone()
        {
            return GetClone();
        }

        public new Bicycle GetClone()
        {
            return new Bicycle(Name, Type, Wheels);
        }

        public override string ToString()
        {
            return base.ToString() + $", wheels number:{Wheels}";
        }
    }
}
