﻿using PrototypePattern.Enums;
using PrototypePattern.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace PrototypePattern.Entities
{
    public class Car : Transport, IMyCloneable<Car>, ICloneable
    {
        public Car(string name, TransportType type, string brandName) : base(name, type)
        {
            BrandName = brandName;
        }

        public string BrandName { get; set; }

        public new object Clone()
        {
            return GetClone();
        }

        public new Car GetClone()
        {
            return new Car(Name, Type, BrandName);
        }

        public override string ToString()
        {
            return base.ToString() + $", brand:{BrandName}";
        }
    }
}
