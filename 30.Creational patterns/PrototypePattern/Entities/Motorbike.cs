﻿using PrototypePattern.Enums;
using PrototypePattern.Interfaces;
using System;

namespace PrototypePattern.Entities
{
    public class Motorbike : Bicycle, IMyCloneable<Motorbike>, ICloneable
    {
        public Motorbike(string name, TransportType type, int wheels, float engineVolume) : base(name, type, wheels)
        {
            EngineVolume = engineVolume;
        }

        public float EngineVolume { get; set; }

        public new object Clone()
        {
            return GetClone();
        }

        public new Motorbike GetClone()
        {
            return new Motorbike(Name, Type, Wheels, EngineVolume);
        }

        public override string ToString()
        {
            return base.ToString() + $", engine volume: {EngineVolume}";
        }
    }
}
