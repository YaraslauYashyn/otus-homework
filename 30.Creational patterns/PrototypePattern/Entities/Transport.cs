﻿using PrototypePattern.Enums;
using PrototypePattern.Interfaces;
using System;

namespace PrototypePattern.Entities
{
    public class Transport: ICloneable, IMyCloneable<Transport>
    {
        public Transport(string name, TransportType type = TransportType.ground)
        {
            Name = name;
            Type = type;
        }

        public virtual string Name { get; set; }

        public virtual TransportType Type { get; set; }

        public object Clone()
        {
            return GetClone();
        }

        public virtual Transport GetClone()
        {
            return new Transport(Name);
        }

        public override string ToString()
        {
            return $"Name:{Name}, type:{Type}";
        }
    }
}
