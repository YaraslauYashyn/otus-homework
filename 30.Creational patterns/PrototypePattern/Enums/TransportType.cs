﻿namespace PrototypePattern.Enums
{
    public enum TransportType
    {
        ground = 1,
        fly = 2,
        water = 3
    }
}
