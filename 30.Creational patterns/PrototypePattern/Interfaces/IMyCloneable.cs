﻿namespace PrototypePattern.Interfaces
{
    public interface IMyCloneable<out T>
    {
        public T GetClone();
    }
}
