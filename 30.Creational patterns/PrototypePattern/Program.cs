﻿using System;

namespace PrototypePattern
{
    class Program
    {
        static void Main()
        {
            TestApp.Test1_UsualCopy();

            TestApp.Test2_DeepCopy();

            TestApp.Test3_DeepCopyWithNesting();

            Console.ReadKey();
        }
    }
}
