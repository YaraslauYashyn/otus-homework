﻿using PrototypePattern.Entities;
using PrototypePattern.Enums;
using System;

namespace PrototypePattern
{
    public static class TestApp
    {
        public static void Test1_UsualCopy()
        {
            Console.WriteLine("Shallow copy of cars");
            var tran1 = new Car("Car", TransportType.ground, "BMW");
            Console.WriteLine("Copying as var transport2 = transport1");
            var tran2 = tran1;
            Console.WriteLine($"Tran 1 : {tran1} == Tran 2 : {tran2} is {tran1.Equals(tran2)}");
            Console.WriteLine("Changing name of transport 1 to New car...");
            tran1.Name = "New car";
            Console.WriteLine($"Tran 1 : {tran1} == Tran 2 : {tran2} is {tran1.Equals(tran2)}");
            Console.WriteLine("Tran 2 still referenced to Tran 1. Shallow copy..." + Environment.NewLine);
        }

        public static void Test2_DeepCopy()
        {
            Console.WriteLine("Deep copy of cars");
            var tran1 = new Car("Car", TransportType.ground, "BMW");
            Console.WriteLine("Copying as var tran2 = tran1.Clone()");
            var tran2 = tran1.Clone();
            Console.WriteLine($"Transport 1 : {tran1} == Transport 2 : {tran2} is {tran1.Equals(tran2)}");
            Console.WriteLine("Changing name of tran 1 to New car...");
            tran1.Name = "New car";
            Console.WriteLine($"Tran 1 : {tran1} == Tran 2 : {tran2} is {tran1.Equals(tran2)}");
            Console.WriteLine("Tran 2 is different to Tran 1. Deep copy..." + Environment.NewLine);
        }

        public static void Test3_DeepCopyWithNesting()
        {
            Console.WriteLine("Deep copy of motorbikes");
            var tran1 = new Motorbike("Moto", TransportType.ground, 2, 50);
            Console.WriteLine("Creating copy as var transport2 = transport1.Clone()");
            var tran2 = tran1.Clone();
            Console.WriteLine($"Tran 1 : {tran1} == Tran 2 : {tran2} is {tran1.Equals(tran2)}");
            Console.WriteLine("Changing engine volume of transport 1 to 60...");
            tran1.EngineVolume = 60;
            Console.WriteLine($"Tran 1 : {tran1} == Tran 2 : {tran2} is {tran1.Equals(tran2)}");
            Console.WriteLine("Tran 2 is different to Tran 1. Deep copy..." + Environment.NewLine);
        }
    }
}
