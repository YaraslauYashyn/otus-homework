﻿using SolidPrinciples.Resources;

namespace SolidPrinciples.AI
{
    class AdvancedAI: BasicAI
    {
        public override bool GiveTip(int setValue, int suggestedValue, out string tip)
        {
            var difference = GetDifference(setValue, suggestedValue);

            tip = string.Empty;

            switch (difference)
            {
                case var n when n > 0 && n <= 10:
                    tip = TextResources.LittleMoreMessage;
                    return false;
                case var n when n > 10 && n <= 30:
                    tip = TextResources.LittleMoreMessage;
                    return false;
                case var n when n > 30:
                    tip = TextResources.LittleMoreMessage;
                    return false;
                case var n when n < 0 && n >= -10:
                    tip = TextResources.LittleMoreMessage;
                    return false;
                case var n when n < -10 && n >= -30:
                    tip = TextResources.LittleMoreMessage;
                    return false;
                case var n when n < -30:
                    tip = TextResources.LittleMoreMessage;
                    return false;
                default:
                    return true;
            }
        }
    }
}
