﻿using SolidPrinciples.Interfaces;
using SolidPrinciples.Resources;

namespace SolidPrinciples.AI
{
    class BasicAI: IAI
    {
        public virtual int GetDifference(int setValue, int suggestedValue)
            => suggestedValue - setValue;

        public virtual bool GiveTip(int setValue, int suggestedValue, out string tip)
        {
            var difference = GetDifference(setValue, suggestedValue);

            tip = string.Empty;

            switch (difference)
            {
                case var n when n > 0:
                    tip = TextResources.UsualMoreMessage;
                    return false;
                case var n when n < 0:
                    tip = TextResources.UsualLessMessage;
                    return false;
                default:
                    return true;
            }
        }
    }
}
