﻿namespace SolidPrinciples.Helpers
{
    static class Validator
    {
        static bool IsInputFilled(string text)
            => !string.IsNullOrEmpty(text);

        static bool IsInputInteger(string balance)
            => int.TryParse(balance, out _);
    }
}
