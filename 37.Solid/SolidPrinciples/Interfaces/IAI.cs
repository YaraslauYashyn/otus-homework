﻿namespace SolidPrinciples.Interfaces
{
    interface IAI
    {
        int GetDifference(int setValue, int suggestedValue);

        bool GiveTip(int setValue, int suggestedValue, out string tip);
    }
}
