﻿namespace SolidPrinciples.Interfaces
{
    interface IPrinter
    {
        void Print(string text);
    }
}
