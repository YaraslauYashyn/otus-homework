﻿namespace SolidPrinciples.Interfaces
{
    interface IRandomizer<T> 
    {
        T Generate(int minValue, int maxValue);
    }
}
