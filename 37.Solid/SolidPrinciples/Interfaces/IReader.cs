﻿namespace SolidPrinciples.Interfaces
{
    interface IReader
    {
        string Read();
    }
}
