﻿using Microsoft.Extensions.Configuration;
using Serilog;
using Serilog.Sinks.SystemConsole.Themes;
using System;
using System.IO;
using System.Threading.Tasks;

namespace SolidPrinciples
{
    class Program
    {
        //На примере реализации игры «Угадай число» продемонстрировать практическое применение SOLID принципов.
        //Программа рандомно генерирует число, пользователь должен угадать это число. При каждом вводе числа программа пишет больше или меньше отгадываемого.
        //Кол-во попыток отгадывания и диапазон чисел должен задаваться из настроек.

        static void Main()
        {
            Log.Logger = new LoggerConfiguration()
                .WriteTo.Console(Serilog.Events.LogEventLevel.Debug, theme: AnsiConsoleTheme.Code)
                .MinimumLevel.Debug()
                .Enrich.FromLogContext()
                .CreateLogger();


            //var builder = new ConfigurationBuilder()
            //    .SetBasePath(Directory.GetCurrentDirectory())
            //    .AddJsonFile("config.json", optional: false);

            //var config = builder.Build();

            //var myFirstClass = config.GetSection("MyFirstClass");
            //var mySecondClass = config.GetSection("MySecondClass");

            try
            {
                MainAsync().GetAwaiter().GetResult();
            }
            catch (Exception ex)
            {
                Log.Warning($"Exception has been thrown during application {ex.Message}");
            }
            finally
            {
                Console.ReadKey();
            }
        }

        static async Task MainAsync()
        {

        }
    }
}
