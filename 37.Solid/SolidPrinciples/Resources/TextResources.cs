﻿namespace SolidPrinciples.Resources
{
    public static class TextResources
    {
        public static string UsualMoreMessage { get; } = "You suggested more than it was set";

        public static string UsualLessMessage { get; } = "You suggested less than it was set";

        public static string LittleMoreMessage { get; } = "You suggested a little bit more than it was set. Try to decrease your mark slightly";

        public static string MediumMoreMessage { get; } = "You suggested more than it was set. Try to decrease your mark moderately";

        public static string BigMoreMessage { get; } = "You suggested much more than it was set. Try to decrease your mark significally";

        public static string LittleLessMessage { get; } = "You suggested a little bit less than it was set. Try to increase your mark slightly";

        public static string MediumLessMessage { get; } = "You suggested less than it was set. Try to increase your mark moderately";

        public static string BigLessMessage { get; } = "You suggested much less than it was set. Try to increase your mark significally";
    }
}
