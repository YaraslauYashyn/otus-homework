﻿using SolidPrinciples.Interfaces;
using System;

namespace SolidPrinciples.Services
{
    class ConsolePrinter : IPrinter
    {
        public void Print(string text)
            => Console.WriteLine();
    }
}
