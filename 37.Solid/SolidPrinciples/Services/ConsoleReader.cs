﻿using SolidPrinciples.Interfaces;
using System;

namespace SolidPrinciples.Services
{
    class ConsoleReader : IReader
    {
        public string Read()
            => Console.ReadLine();
    }
}
