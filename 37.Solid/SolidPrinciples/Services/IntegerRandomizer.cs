﻿using SolidPrinciples.Interfaces;
using System;

namespace SolidPrinciples.Services
{
    class IntegerRandomizer : IRandomizer<int>
    {
        public static Random GetRandom()
            => new(Guid.NewGuid().GetHashCode());

        public int Generate(int minValue, int maxValue)
            => GetRandom().Next(minValue, maxValue);
    }
}
